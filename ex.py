# -------------------------------------------------------#
# Author    : Computer for Education                     #
# Author URI: https://www.computerforedu.com             #
# Facebook  : https://www.facebook.com/computerforedu    #
# -------------------------------------------------------#
 
max_num = 9
n = 0;
while n < max_num:
    n += 1
    for s in range(n):
        print(" ", end=' ')
 
    print(n)